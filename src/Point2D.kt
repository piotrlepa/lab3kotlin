open class Point2D(
    val x: Double,
    val y: Double
) {
    override fun toString(): String = "x: $x, y: $y"
}