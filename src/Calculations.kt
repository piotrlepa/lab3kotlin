class Calculations {

    companion object {
        fun positionGeometricCenter(points: List<Point2D>): Point2D {
            val size = points.size
            val x = points.sumByDouble { it.x } / size
            val y = points.sumByDouble { it.y } / size
            return Point2D(x, y)
        }

        fun positionCenterOfMass(points: List<MaterialPoint2D>): Point2D {
            val x = points.sumByDouble { it.x * it.mass }
            val y = points.sumByDouble { it.y * it.mass }
            val mass = points.sumBy { it.mass }
            return MaterialPoint2D(x / mass, y / mass, mass)
        }
    }
}