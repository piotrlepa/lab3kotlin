class MaterialPoint2D(
    x: Double,
    y: Double,
    val mass: Int
) : Point2D(x, y) {

    override fun toString(): String = super.toString() + ", mass: $mass"
}